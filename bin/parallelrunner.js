#!/usr/bin/env node

'use strict';

const program = require('commander');
const version = require('../package.json').version;
const mainApp = require('../lib/index');
 
program.name = 'parallelrunner';
program
  .version(version)
  .option('-p, --path <path>','folder Path')
  .parse(process.argv);

if(program.path) {
    mainApp.run(program.path);
} else {
    console.log("Please enter the respective collections folder path");
}