# Parallelrunner
Parallelrunner is a simple command line tool which executes the postman collection parallelly.
The process is very simple. Just download the collections and the respective environment in the below-given folder format and call the parallelrunner application. Parallelrunner will execute all the collections parallelly in the desired folder and generates custom HTML report parallel to your collections.

# New Features!

  - The execution time is very fast as it runs all the collections parallelly
  - Can able to run the collections based on some simple priority
  - Custom HTML report will be generated in parallel to collections location

### Tech

Parallelrunner uses a number of open source projects to work properly:

* [node.js] - Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine
* [newman] - the cli companion for postman

### Installation

To run parallelrunner, ensure that you have NodeJS >= v8.5.0
The easiest way to install parallelrunner is using NPM. If you have NodeJS installed, it is most likely that you have NPM installed as well.

```sh
$ npm install parallelrunner -g
```

### Execution

```sh
$ parallelrunner -p <path to postman collection folder>
```
- `-p <path>`, `--path <path>`  Specify the folderpath.
Example:-
```sh
$ parallelrunner -p /home/dir/collections
```

### Usage

##### Simple Case
* Create a folder in your system wherever you want
* Download Collections from postman and place it in the newly created folder
    Example : peoplemgmtCollections.json
* Download Environment from the postman and place it in the newly created folder parallel to the downloaded collections.
* Append Env to last of the environment json file
    Example: peoplemgmtCollectionsEnv.json
* Download Global Environment from the postman and place it in the newly created folder parallel to the downloaded collections. < optional >
* Append GlobalEnv to last of the global environment json file
    Example: peoplemgmtCollectionsGlobalEnv.json
* Then call the parallelrunner, it will execute the collections with its respective environment
* The final report will be generated and placed inside the reports folder, which will be created parallelly to the collections at runtime.

[simple folder structure]

##### One Environment with multiple collections
* Create a folder in your system wherever you want
* Download Environment form the postman and place it in the newly created folder
* Append Env to last of the environment json file
    Example: peoplemgmtCollectionsEnv.json
* Create a sub-folder inside the newly created folder
* Download all the collections from the postman and place it in newly created sub-folder
* Then call the parallelrunner, it will execute all the collections in the sub-folder with the single environment.
* The final report will be generated and placed inside the reports folder, which will be created parallelly to the collections at runtime.

[Multiple collections folder structure 1]

##### Priority based execution
* Create a folder in your system wherever you want
* Download Collections from postman and place it in the newly created folder
    Example : peoplemgmtCollections.json
* Download Environment from the postman and place it in the newly created folder parallel to the downloaded collections.
* Append Env to last of the environment json file
    Example: peoplemgmtCollectionsEnv.json
* Download Global Environment from the postman and place it in the newly created folder parallel to the downloaded collections. < optional >
* Append GlobalEnv to last of the global environment json file
    Example: peoplemgmtCollectionsGlobalEnv.json
* Create a sub-folder inside the newly created folder
* Download all the collections from the postman and place it in newly created sub-folder
* Then call the parallelrunner, the parallelrunner first execute the collections defined in the parent-folder and based on the result formed at runtime, the collections in the sub-folder get executed.
* The final report will be generated and placed inside the reports folder, which will be created parallelly to the collections at runtime.

[Priority based folder structure]


License
----

ISC

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [node.js]: <http://nodejs.org>
   [newman]: <https://github.com/postmanlabs/newman>
   [simple folder structure]: <https://bitbucket.org/satheeshv25/parallelrunner/annotate/master/images/simple.png?fileviewer=file-view-default>
   [Multiple collections folder structure 1]: <https://bitbucket.org/satheeshv25/parallelrunner/annotate/master/images/multipleCollections.png?fileviewer=file-view-default>
   [Priority based folder structure]: <https://bitbucket.org/satheeshv25/parallelrunner/annotate/master/images/priorityBased.png?fileviewer=file-view-default>