const glob = require('glob');
const fs = require('fs');

exports.readdir = function (folderPath, callback) {
    fs.readdir(folderPath, (err, items) => {
        if (err) {
            console.log(`Error in reading directory ${folderPath} `, err);
            callback(err, null);
        } else {
            const files = items.filter(x => x.includes('.json'));
            const collectionFiles = files.filter(x => !x.includes('Env.json')).filter(x => !x.includes('GlobalEnv.json')).map(x => `${folderPath}/${x}`);
            const envFile = files.filter(x => x.includes('Env.json')).map(x => `${folderPath}/${x}`);
            const globalEnv = files.filter(x => x.includes('GlobalEnv.json')).map(x => `${folderPath}/${x}`);
            const subFolders = items.filter(x => !x.includes('.json')).filter(x => !x.includes('reports')).map(x => `${folderPath}/${x}`);
            callback(null, {
                "collectionFiles": collectionFiles,
                "envFile": envFile,
                "globalEnv" : globalEnv,
                "subFolders": subFolders
            });
        }
    });
}