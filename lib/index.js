const async = require('async');
const runner = require('./runner');
const fileHandler = require('./fileHandler');
const _this = this;
const dateFormat = require('dateformat');
const _ = require('underscore');

exports.run = function (folderPath) {
    function callback(err, results) {
        if (err) {
            console.log(`Error Encountered in parallel runner while running collections`);
        } else {
            console.log(`parallel runner completed it task successfully`);
        }
    };
    const commonEnvs = {
        'env': undefined,
        'globalEnv': undefined
    }
    parallelRun(folderPath, commonEnvs, callback);
}

function parallelRun(folderPath, commonEnvs, callback) {
    async.auto({
        readdir: (callback) => {
            fileHandler.readdir(folderPath, callback);
        },
        commonCollectionValidation: ['readdir', (results, callback) => {
            commonCollectionValidation(commonEnvs, results, callback);
        }],
        commonCollectionRun: ['commonCollectionValidation', (results, callback) => {
            commonCollectionRun(results, callback);
        }],
        folders: ['commonCollectionRun', (results, callback) => {
            folders(commonEnvs, results, callback);
        }]
    }, (err, results) => {
        if (err) {
            console.log(`Error in running folder ${folderPath}`, err);
            callback(err, null);
        } else {
            console.log(`completed execution in folder ${folderPath}`);
            callback(null, true);
        }
    });
}

function commonCollectionValidation(commonEnvs, results, callback) {

    const directoryContent = results.readdir;
    if (directoryContent != null) {
        if (directoryContent.collectionFiles.length > 0) {
            const collectionEnvPair = hasCollections(directoryContent, commonEnvs);
            callback(null, collectionEnvPair);
        } else if (directoryContent.collectionFiles.length === 0 && directoryContent.envFile.length > 1) {
            console.log(`More then one Env files ${directoryContent.envFile} is encountered`);
            callback(true, null);
        } else if (directoryContent.collectionFiles.length === 0 && directoryContent.globalEnv.length > 1) {
            console.log(`More then one globalEnv files ${directoryContent.globalEnv} is encountered`);
            callback(true, null);
        } else {
            hasOnlyEnv(commonEnvs, directoryContent);
            callback(null, undefined);
        }
    } else {
        callback(null, undefined);
    }
}

function hasOnlyEnv(commonEnvs, directoryContent) {
    if (directoryContent.envFile[0] != null) {
        commonEnvs.env = directoryContent.envFile[0];
    }

    if (directoryContent.globalEnv[0] != null) {
        commonEnvs.globalEnv = directoryContent.globalEnv[0];
    }
}

function hasCollections(directoryContent, commonEnvs) {
    const collectionEnvPair = [];
    directoryContent.collectionFiles.forEach(function (collection) {
        const collectionEnvFile = collection.replace('.json', 'Env.json');
        const collectionGlobalEnvFile = collection.replace('.json', 'GlobalEnv.json');
        const reportLocation = reportLocationGenerator(collection);
        if (directoryContent.envFile.indexOf(collectionEnvFile) != -1) {
            collectionEnvPair.push({
                'collection': collection,
                'env': directoryContent.envFile[directoryContent.envFile.indexOf(collectionEnvFile)],
                'globalEnv': directoryContent.globalEnv[directoryContent.globalEnv.indexOf(collectionGlobalEnvFile)] || commonEnvs.globalEnv,
                'reportLocation': reportLocation
            });
        } else {
            if (commonEnvs != null && commonEnvs.env != null) {
                collectionEnvPair.push({
                    'collection': collection,
                    'env': commonEnvs.env,
                    'globalEnv': commonEnvs.globalEnv,
                    'reportLocation': reportLocation
                });
            }
        }
    });
    return collectionEnvPair;
}

function reportLocationGenerator(collection) {
    let valueArray = collection.split("/");
    valueArray.splice(valueArray.length - 1, 0, 'reports');
    return valueArray.join("/").replace('.json', `Report_${dateFormat(new Date(), 'ddmmmyyhhMMss')}.html`);
}

function commonCollectionRun(results, callback) {
    if (results.commonCollectionValidation != null && results.commonCollectionValidation.length > 0) {
        runner.runCollections(results.commonCollectionValidation, callback);
    } else {
        callback(null, undefined);
    }
}

function folders(commonEnvs, results, callback) {
    if (results.commonCollectionRun != null) {
        runFolders(results.readdir.subFolders, results.commonCollectionRun, callback);
    } else {
        runFolders(results.readdir.subFolders, commonEnvs, callback);
    }
}

function runFolders(subfolders, envs, cb) {
    if (subfolders != null && subfolders.length > 0) {
        async.map(subfolders, (folder, callback) => {
            parallelRun(folder, envs, callback);
        }, (err, results) => {
            if (err) {
                console.log(`Error in running subFolders`, err);
                cb(err, null);
            } else {
                cb(null, true);
            }
        });
    } else {
        cb(null, true);
    }
}