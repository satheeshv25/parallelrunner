const newman = require('newman');
const async = require('async');
const path = require('path');
const _ = require('underscore');

exports.runCollections = ( collectionObject, cb) => {
    if (collectionObject != null && collectionObject.length > 0) {
        async.map(collectionObject, (collectionObj, callback) => {
            const newmanObject = {
                collection: collectionObj.collection,
                environment: collectionObj.env,
                globals: collectionObj.globalEnv,
                reporters: 'html',
                reporter: {
                    html: {
                        export: collectionObj.reportLocation,
                        template: path.join(__dirname, './series.hbs')
                    }
                },
                timeout: 1800000
            };
            this.run(newmanObject, callback);
        }, (err, results) => {
            if (err) {
                console.log(`Error in running collections`, err);
                cb(err, null);
            } else {
                let env = [];
                let globalEnv = [];
                results.forEach((result) => {
                    env = _.flatten(env.concat(result.env));
                    globalEnv = _.flatten(globalEnv.concat(result.globalEnv));
                });
                cb(null, {
                    "env" : envsformation('1', 'Runtime Formed Environment', env, 'environment'),
                    "globalEnv" : envsformation('2', 'Runtime Formed Global Environment', globalEnv, 'globals')
                });
            }
        });
    } else {
        console.log(`No collections ${collectionObject} to run in folder ${folderPath}`);
        cb(null, undefined);
    }
}

function envsformation(id, name, values, scope) {
    return {
        "id": id,
        "name": name,
        "values": _.flatten(values),
        "_postman_variable_scope": scope,
        "_postman_exported_at": new Date().toISOString(),
        "postman_exported_using": "Postman/5.5.0",
    }
}

exports.run = (newmanObject, callback) => {
    newman.run(newmanObject, function (err) {
        if (err) {
            console.log(`Error in newman execution ${newmanObject.collection}`);
            callback(err, null);
        }
    }).on('start', function (err, args) {
        if (err) {
            console.log(`Error in starting the newman ${newmanObject.collection}`);
            callback(err, null);
        } else {
            console.log(`${newmanObject.collection} execution started`);
        }
    }).on('done', function (err, summary) {
        if (err || summary.error) {
            console.error(`Error in newman execution with fileName ${newmanObject.collection}`);
            callback(err, null);
        } else {
            console.log(`${newmanObject.collection} run completed`);
            const env = summary.environment.values.members
            const globals = summary.globals.values.members
            callback(null, {
                "env": env,
                "globalEnv": globals
            });
        }
    });
}